# Official 7.0 (Nougat) firmware image
Made possible by [this XDA post](https://forum.xda-developers.com/moto-x-style/general/xt1575-moto-x-pure-edition-factory-t3704142). This image is intended to be installed with `fastboot`. Download and unzip the official image file. Connect your phone to your computer. Verify `fastboot` is detecting your device.
```
$ fastboot devices
List of devices attached
AB123C456D      fastboot
```
If the device is detected correctly, issue the following commands from your computer:
```
fastboot oem lock begin # or `fastboot oem fb_mode_set` if you don't want to re-lock bootloader
fastboot flash partition gpt.bin
fastboot flash bootloader bootloader.img
fastboot flash logo logo.bin
fastboot flash boot boot.img
fastboot flash recovery recovery.img
fastboot flash system system.img_sparsechunk.0
fastboot flash system system.img_sparsechunk.1
fastboot flash system system.img_sparsechunk.2
fastboot flash system system.img_sparsechunk.3
fastboot flash system system.img_sparsechunk.4
fastboot flash system system.img_sparsechunk.5
fastboot flash system system.img_sparsechunk.6
fastboot flash system system.img_sparsechunk.7
fastboot flash system system.img_sparsechunk.8
fastboot flash system system.img_sparsechunk.9
fastboot flash modem NON-HLOS.bin
fastboot erase modemst1 
fastboot erase modemst2
fastboot flash fsg fsg.mbn
fastboot flash bluetooth BTFM.bin
fastboot erase cache
fastboot erase userdata
fastboot erase customize
fastboot erase clogo
fastboot oem lock # or `fastboot oem fb_mode_clear` if you don't want to re-lock bootloader
fastboot reboot
```